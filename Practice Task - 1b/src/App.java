
public class App {

	public static void main(String[] args) {
		Car car = new Car("Ford");
		System.out.println(car.toString());
		
		Car newCar = new Car ("Dodge"){
			public String toString() {
				return "newCar [name=" + name + "]";
			}
		};
		System.out.println(newCar.toString());
		Car ladaCar = new Car ("Lada"){
			public String toString() {
				return "LadaCar [name=" + name + "]";
			}
		};
		System.out.println(ladaCar.toString());

	}

}
